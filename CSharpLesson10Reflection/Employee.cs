﻿

namespace CSharpLesson10Reflection
{
    public class Employee : Person
    {
        public Position Position { get; set; }
        public double Salary { get; set; }

    }
}

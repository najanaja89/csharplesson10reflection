﻿

namespace CSharpLesson10Reflection
{
    public interface IEmployeeService
    {
        void Up(Employee employee);
        void Down(Employee employee);
        bool Fire(Employee employee);
    }
}

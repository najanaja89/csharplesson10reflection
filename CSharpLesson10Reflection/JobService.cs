﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLesson10Reflection
{
    class JobService : IEmployeeService
    {
        void IEmployeeService.Down(Employee employee)
        {
            throw new NotImplementedException();
        }

        bool IEmployeeService.Fire(Employee employee)
        {
            return true;
        }

        void IEmployeeService.Up(Employee employee)
        {
            Console.WriteLine(employee.Name + " повышен");
        }
    }
}

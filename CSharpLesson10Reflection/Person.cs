﻿using System;


namespace CSharpLesson10Reflection
{
    public abstract class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }

        public Person()
        {
            Id = Guid.NewGuid();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CSharpLesson10Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            IEmployeeService service = new JobService();
            Employee vasja = new Employee
            {
                Name = "Вася",
                Position = Position.Vasya,
                Salary = 0.4,
                BirthDate = null
            };

            Employee manager = new Employee
            {
                Name = "Тулеген",
                Position = Position.Vasya,
                Salary = 99999.6,
                BirthDate = DateTime.Now
            };

            service.Fire(vasja);
            service.Up(manager);

            Console.ReadLine();
        }
    }
}
